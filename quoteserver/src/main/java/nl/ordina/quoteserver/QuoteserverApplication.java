package nl.ordina.quoteserver;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class QuoteserverApplication {

    public static void main(String[] args) {
        SpringApplication.run(QuoteserverApplication.class, args);
    }

}

@RestController
@RequiredArgsConstructor
class QuoteController {

    private final QuoteRepository quoteRepository;

    @GetMapping
    public List<Quote> getQuote() {
        return quoteRepository.findAll();
    }
    
    @PostMapping
    public void postQuote(@RequestParam String text) {
        quoteRepository.save( new Quote(null, text));
    }
}

interface QuoteRepository extends JpaRepository<Quote, Long> {
}

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
class Quote {

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private Long id;
    private String text;

}
