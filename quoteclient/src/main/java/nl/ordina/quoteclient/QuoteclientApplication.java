package nl.ordina.quoteclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class QuoteclientApplication {

    public static void main(String[] args) {
        SpringApplication.run(QuoteclientApplication.class, args);
    }

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();

    }

}

@RestController
class QuoteController {

    private final RestTemplate restTemplate;

    public QuoteController(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @GetMapping
    public ResponseEntity<Quote> getQuote() {

        ResponseEntity<Quote[]> response
                = restTemplate.getForEntity(
                        "http://localhost:8080/",
                        Quote[].class);
        Quote[] quotes = response.getBody();
        
        if (quotes == null || quotes.length <=0 ) {
            return ResponseEntity.noContent().build();
        }
            
        return ResponseEntity.ok(quotes[0]);
    }
}

class Quote {

    String id;
    String text;

    public Quote(String id, String text) {
        this.id = id;
        this.text = text;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
